#!/bin/bash

set -euxo pipefail

cat config

wget https://github.com/coreos/container-linux-config-transpiler/releases/download/v0.9.0/ct-v0.9.0-x86_64-unknown-linux-gnu
chmod +x ct-v0.9.0-x86_64-unknown-linux-gnu
mv ct-v0.9.0-x86_64-unknown-linux-gnu ct
./ct -strict -pretty -in-file config -out-file config.ign

cat config.ign

apt-get install dc
# a164d8785cf49d69091bfec642ed59cc59bb3421 = master of https://github.com/coreos/init at Di 1. Okt 09:33:49 CEST 2019
wget https://raw.githubusercontent.com/coreos/init/a164d8785cf49d69091bfec642ed59cc59bb3421/bin/coreos-install
chmod +x coreos-install
./coreos-install -d /dev/nvme0n1 -i config.ign -C stable
