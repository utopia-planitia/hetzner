#!/bin/bash

set -euxo pipefail

cat config

wget https://github.com/coreos/container-linux-config-transpiler/releases/download/v0.9.0/ct-v0.9.0-x86_64-unknown-linux-gnu
chmod +x ct-v0.9.0-x86_64-unknown-linux-gnu
mv ct-v0.9.0-x86_64-unknown-linux-gnu ct
./ct -strict -pretty -in-file config -out-file /usr/share/oem/config.ign

touch /boot/coreos/first_boot
touch /var/run/update_engine_autoupdate_completed
