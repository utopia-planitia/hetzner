
DOCKER_OPTIONS += -v $(ANSIBLE_PATH)/files:/workspace/files

include ../kubernetes/etc/help.mk
include ../kubernetes/etc/cli.mk
include ./etc/tests.mk

#ANSIBLE_OPTIONS ?= --limit=

# Set colors
cc_red=$(shell echo "\033[0;31m")
cc_green=$(shell echo "\033[0;32m")
cc_yellow=$(shell echo "\033[0;33m")
cc_blue=$(shell echo "\033[0;34m")
cc_normal=$(shell echo "\033[m\017")


.PHONY: reinstall
reinstall: ##@setup (re)install OS
	@echo "${cc_yellow}please verify and execute:${cc_normal}"
	@echo $(CLI) ansible-playbook reinstall.yml $(ANSIBLE_OPTIONS)

.PHONY: delete-all-nodes-reinstall-i-really-mean-it
delete-all-nodes-reinstall-i-really-mean-it: ##@setup (re)install OS without any more questions
	$(CLI) ansible-playbook reinstall.yml $(ANSIBLE_OPTIONS)

.PHONY: reboot
reboot: ##@setup restart hardware
	$(CLI) ansible-playbook reboot.yml $(ANSIBLE_OPTIONS)

.PHONY: maintenance
maintenance: ##@setup update nodes
	$(CLI) ansible-playbook maintenance.yml $(ANSIBLE_OPTIONS)
